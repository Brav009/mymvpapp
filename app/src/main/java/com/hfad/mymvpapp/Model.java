package com.hfad.mymvpapp;

public class Model implements IContract.IModel {

    @Override
    public String getCalculatedResult() {
        double randNumberOne = Math.random();
        double randNumberTwo = Math.random();
        double a = randNumberOne * 100;
        double b = randNumberTwo * 100;
        double s = a + b;
        return Double.toString(s);
    }
}
