package com.hfad.mymvpapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements IContract.IView {

    IContract.IPresenter myMVPPresenter;
    TextView tvCalculate;
    Button btnCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvCalculate = findViewById(R.id.tvCalculate);
        btnCalculate = findViewById(R.id.btnCalculate);
        myMVPPresenter = new Presenter(this);


        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myMVPPresenter.calculate();
            }
        });


    }

    @Override
    public void showCalculatedResult(String calculated) {
        tvCalculate.setText(calculated);

    }
}