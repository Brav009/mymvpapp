package com.hfad.mymvpapp;

public interface IContract {

    interface IView {
        void showCalculatedResult(String calculate);
    }

    interface IPresenter {
        void calculate();
    }

    interface IModel {
        String getCalculatedResult();
    }
}
