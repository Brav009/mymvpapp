package com.hfad.mymvpapp;

public class Presenter implements IContract.IPresenter {

    IContract.IView myMVPView;
    IContract.IModel myMVPModel;

    public Presenter(IContract.IView view) {
        this.myMVPView = view;
        this.myMVPModel = new Model();
    }

    @Override
    public void calculate() {
        String resCalculated = myMVPModel.getCalculatedResult();
        myMVPView.showCalculatedResult(resCalculated);


    }
}
